<?php
function calcular_aleatorio($xn, $a, $c, $m, $n)
{
	$nx = 0;
    $datos = [];

    for ($i=0; $i < $n; $i++) {
        $nx = $nx + 1;
        $arreglo[0] = $nx;
        $arreglo[1] = $xn;
        $aporxn = ($a*$xn)+$c;
        $arreglo[2] = $aporxn;
        $modulo1 = ($aporxn/$m);
        $modulo = ($modulo1 - intval($modulo1)) * $m;
        $arreglo[3] = round($modulo);
        $ri = round(($modulo1 - intval($modulo1)),2);
        $arreglo[4] = $ri;
        $xn = round($modulo);
        array_push($datos, $arreglo);
    }

    return $datos;
}
function buscar_en_pro($arreglo, $numero)
{
    $bandera = 0;
    $valor_final = null;
    for ($i=0; $i < count($arreglo); $i++) { 
        $rango_menor = $arreglo[$i][3];
        $rango_mayor = $arreglo[$i][4];
        if ($numero >= $rango_menor && $numero <= $rango_mayor) {
            $valor_final = $arreglo[$i][0];
        }
    }
    return $valor_final;
}

function maximo($valor_1, $valor_2)
{
    $arreglo[0] = $valor_1;
    $arreglo[1] = $valor_2;
    $max = max($arreglo);
    return $max;
}