<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class MD_user_normal
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            if (Auth::check())
    {
                
        $useractual=\Auth::user();
        if ($useractual->tipo_usuario_id!=2&&$useractual->tipo_usuario_id!=3) {
              return view('errors.503');           

        }
        return $next($request);
    }else{
           return view('errors.503');      
       }



    }
}
