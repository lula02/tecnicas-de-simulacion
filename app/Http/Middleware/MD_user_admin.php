<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class MD_user_admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $nextMD_user_admin
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            if (Auth::check())
    {
                
        $useractual=\Auth::user();
       if ($useractual->id_tipo_usuario != 3) {
              return view('errors.503');

        }
        return $next($request);
    }else{
           return view('errors.503');      
       }



    }
}
