<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MetodosPronosticosController extends Controller
{
    public function index_pm(){ //Vista Promedio Movil
    	return view('MetodosPronosticos.promedio_movil');
    }

    public function index_ae(){ //Vista de Alisamiento Exponencial
    	return view('MetodosPronosticos.alisamiento_expo');
    }

    public function index_rl(){ //Vista de Regresion lineal
    	return view('MetodosPronosticos.regresion_lineal');
    }
}
