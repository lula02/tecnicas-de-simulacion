<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class inventario extends Controller
{
    public function index()
    {
    	return view('inicio.inventariomontecarlo');
    }
    public function inventario(Request $request)
    {
        $r = $request->r;
        $q = $request->q;
        $inv_ini = $request->inv_ini;
        $ch = $request->ch;
        $co = $request->Co;
        $cf = $request->cf;

        $tabla_pro_demanda = [];
        $acumulador_pro_dem = 0;
        $acumulador_rango_menor = 0;
        for ($i=0; $i < count($request->valor3); $i++) { 
            $tabla_pro_demanda[$i][0] = $request->valor3[$i];
            $tabla_pro_demanda[$i][1] = $request->probabilidad3[$i];
            $acumulador_pro_dem = $acumulador_pro_dem + $request->probabilidad3[$i];
            $tabla_pro_demanda[$i][2] = $acumulador_pro_dem;
            $tabla_pro_demanda[$i][3] = round($acumulador_rango_menor,3);
            $acumulador_rango_menor = round($acumulador_pro_dem,3) + 0.001;
            $tabla_pro_demanda[$i][4] = $acumulador_pro_dem;
        }

        $tabla_pro_entrega = [];
        $acumulador_pro_ent = 0;
        $acumulador_rango_menor = 0;
        for ($i=0; $i < count($request->valor4); $i++) { 
            $tabla_pro_entrega[$i][0] = $request->valor4[$i];
            $tabla_pro_entrega[$i][1] = $request->probabilidad4[$i];
            $acumulador_pro_ent = $acumulador_pro_ent + $request->probabilidad4[$i];
            $tabla_pro_entrega[$i][2] = $acumulador_pro_ent;
            $tabla_pro_entrega[$i][3] = round($acumulador_rango_menor,3);
            $acumulador_rango_menor = round($acumulador_pro_ent,3) + 0.001;
            $tabla_pro_entrega[$i][4] = $acumulador_pro_ent;
        }

        $tabla_calculada = [];
        $contador_res = 1;
        $hora_llegada_exacta = 0;
        $hora_fin_servicio = 0;
        $marcador_entrega = 0;
        for ($i=0; $i < count($request->aleatorio_llegada); $i++) { 
            $tabla_calculada[$i][0] = $contador_res;
            $tabla_calculada[$i][1] = $request->aleatorio_llegada[$i];
            $deandas_ventas = buscar_en_pro($tabla_pro_demanda, $request->aleatorio_llegada[$i]);
            $tabla_calculada[$i][2] = $deandas_ventas;
            $tabla_calculada[$i][3] = $inv_ini;
            $inv_tem = $inv_ini;
            $inv_ini = $tabla_calculada[$i][3] - $tabla_calculada[$i][2];
            if ($inv_ini < 0) {
                $inv_ini = 0;
            }
            $tabla_calculada[$i][4] = 0;
            if ($marcador_entrega != 0) {
                if ($tabla_calculada[$i][0] == $marcador_entrega) {
                    $tabla_calculada[$i][4] = $q;
                    $inv_ini = $inv_ini + $q - $tabla_calculada[$i][2] + $inv_tem;
                    $marcador_entrega = 0;
                }
            }

            $tabla_calculada[$i][5] = $inv_ini;
            $costo_faltante = 0;
            if ($tabla_calculada[$i][2] > ($tabla_calculada[$i][3] + $tabla_calculada[$i][4])) {
            $costo_faltante = $cf;
            }
            $tabla_calculada[$i][6] = $costo_faltante;

            $costo_mantener=0;
            $costo_mantener = $ch * $tabla_calculada[$i][5];
            $tabla_calculada[$i][7] = $costo_mantener;

            $orden = 0;
            if ($marcador_entrega == 0) {
                if ($tabla_calculada[$i][5] <= $r) {
                    $orden = $co;
                }
            }
            $tabla_calculada[$i][8] = $orden;

            $tabla_calculada[$i][9] = $request->aleatorio_servicio[$i];
            $t_entrega = 0;
            if ($marcador_entrega == 0) {
                if ($tabla_calculada[$i][5] <= $r) {
                    $t_entrega = buscar_en_pro($tabla_pro_entrega, $request->aleatorio_servicio[$i]);
                }
            }

            $tabla_calculada[$i][10] = $t_entrega;
            $dia_entrega = 0;
            if ($marcador_entrega == 0) {
                if ($tabla_calculada[$i][10] > 0) {
                    $dia_entrega = $tabla_calculada[$i][10] + $tabla_calculada[$i][0] + 1;
                }
            }
            $tabla_calculada[$i][11] = $dia_entrega;
            if ($tabla_calculada[$i][11] != 0) {
                $marcador_entrega = $tabla_calculada[$i][11];
            }
            $contador_res = $contador_res + 1;
        }
        return view('desnudas.inv_mon_resul',compact('tabla_pro_demanda','tabla_pro_entrega','tabla_calculada'));
    }
}
