<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class lineaespera extends Controller
{
    public function index()
    {
        return view('inicio.lineaespera');
    }


    public function generar_aleatorios(Request $request)
    {
        $aleatorios_llegada = [];
        $eventos = $request->eventos;
        for ($i=0; $i < $eventos; $i++) { 
            $aleatorio = 1000/rand(1,1000);
            $randomx = round(($aleatorio - intval($aleatorio)),3);
            array_push($aleatorios_llegada,$randomx);
        }

        $aleatorios_servicio = [];
        for ($i=0; $i < $eventos; $i++) { 
            $aleatorio = 1000/rand(1,1000);
            $randomx = round(($aleatorio - intval($aleatorio)),3);
            array_push($aleatorios_servicio,$randomx);
        }
        return view('desnudas.aleatorios_llegada_servicio',compact('aleatorios_llegada','aleatorios_servicio'));
    }


    public function calcular_linea_espera(Request $request)
    {
        $llegadas = $request->aleatorio_llegada;
        $servicios = $request->aleatorio_servicio;
        $contador = 0;
        $coun = 0;
        $lamda = $request->lamda;
        $niu = $request->niu;
        $datos = [];

        $fila[0] = $contador;
        $fila[1] = ' - ';
        $fila[2] = ' - ';
        $fila[3] = ' - ';
        $fila[4] = ' - ';
        $fila[5] = 0;
        $fila[6] = 0;
        $fila[7] = 0;
        $fila[8] = 0;
        $fila[9] = 0;
        array_push($datos,$fila);
        for ($i=0; $i < count($llegadas); $i++) {
            $contador = $contador + 1;
            $fila[0] = $contador;
            $llegada = $llegadas[$i];
            $fila[1] = $llegada;
            $servicio = $servicios[$i];
            $fila[2] = $servicio;
            $entre_llegadas = (-1/$lamda)*log($llegada);
            $fila[3] = round($entre_llegadas,2);
            $t_servicio = (-1/$niu)*log($servicios[$i]);
            $fila[4] = round($t_servicio,2);
            $llegada_exacta = round($datos[$i][5] + $entre_llegadas,2);
            $fila[5] = $llegada_exacta;
            $inicio_servicio = max($llegada_exacta,$datos[$i][7]);
            $fila[6] = round($inicio_servicio,2);
            $fin_servicio = round($inicio_servicio + $t_servicio,2);
            $fila[7] = $fin_servicio;
            $t_espera = round($inicio_servicio - $llegada_exacta,2);
            $fila[8] = $t_espera;
            $t_sistema = round($t_espera + $t_servicio,2);
            $fila[9] = $t_sistema;
            array_push($datos, $fila);
        }

     $vistas = view('desnudas.lineaesperatabla',compact('datos','contador'))->render();
        return [$vistas,$datos,$contador];
    }


    public function montecarlo()
    {
        return view('inicio.lineaesperamontecarlo');
    }

    public function generar_tab_pro(Request $request)
    {
        $valor_n = $request->cantidad;
        $tipo = $request->tipo;
        return view('desnudas.tabla_tab_pro',compact('valor_n','tipo'));
    }

        public function calcularlineamontecarlo(Request $request)
    {
        $tabla_pro_llegada = [];
        $acumulador_pro_lle = 0;
        $acumulador_rango_menor = 0;
        for ($i=0; $i < count($request->valor1); $i++) { 
            $tabla_pro_llegada[$i][0] = $request->valor1[$i];
            $tabla_pro_llegada[$i][1] = $request->probabilidad1[$i];
            $acumulador_pro_lle = $acumulador_pro_lle + $request->probabilidad1[$i];
            $tabla_pro_llegada[$i][2] = $acumulador_pro_lle;
            $tabla_pro_llegada[$i][3] = round($acumulador_rango_menor,3);
            $acumulador_rango_menor = round($acumulador_pro_lle,3) + 0.001;
            $tabla_pro_llegada[$i][4] = $acumulador_pro_lle;
        }

        $tabla_pro_servicio = [];
        $acumulador_pro_ser = 0;
        $acumulador_rango_menor_ser = 0;
        for ($i=0; $i < count($request->valor2); $i++) { 
            $tabla_pro_servicio[$i][0] = $request->valor2[$i];
            $tabla_pro_servicio[$i][1] = $request->probabilidad2[$i];
            $acumulador_pro_ser = $acumulador_pro_ser + $request->probabilidad2[$i];
            $tabla_pro_servicio[$i][2] = $acumulador_pro_ser;
            $tabla_pro_servicio[$i][3] = round($acumulador_rango_menor_ser,3);
            $acumulador_rango_menor_ser = round($acumulador_pro_ser,3) + 0.001;
            $tabla_pro_servicio[$i][4] = $acumulador_pro_ser;
        }

        $tabla_calculada = [];
        $contador_res = 1;
        $hora_llegada_exacta = 0;
        $hora_fin_servicio = 0;
        for ($i=0; $i < count($request->aleatorio_llegada); $i++) { 
            $tabla_calculada[$i][0] = $contador_res;
            $tabla_calculada[$i][1] = $request->aleatorio_llegada[$i];
            $tabla_calculada[$i][2] = $request->aleatorio_servicio[$i];
            $t_entre_llegadas = buscar_en_pro($tabla_pro_llegada, $request->aleatorio_llegada[$i]);
            $tabla_calculada[$i][3] = $t_entre_llegadas;
            $t_servicio = buscar_en_pro($tabla_pro_servicio, $request->aleatorio_servicio[$i]);
            $tabla_calculada[$i][4] = $t_servicio;
            $tabla_calculada[$i][5] = $hora_llegada_exacta + $tabla_calculada[$i][3];
            $hora_llegada_exacta = $hora_llegada_exacta + $tabla_calculada[$i][3];
            $tabla_calculada[$i][6] = maximo($tabla_calculada[$i][5],$hora_fin_servicio);
            $hora_fin_servicio = $tabla_calculada[$i][6] + $tabla_calculada[$i][4];
            $tabla_calculada[$i][7] = $hora_fin_servicio;
            $tabla_calculada[$i][8] = $tabla_calculada[$i][6] - $tabla_calculada[$i][5];
            $tabla_calculada[$i][9] = $tabla_calculada[$i][8] + $tabla_calculada[$i][4];
            $contador_res = $contador_res + 1;
        }

        return view('desnudas.lineaesperamontecalo',compact('tabla_pro_llegada','tabla_pro_servicio','tabla_calculada'));
    }
}
