<div class="col-md-12">
	<label>Números Aleatorios</label>
	<table class="table" id="llegada">
		<?php $contador = 0;?>
		@for ($i = 0; $i < count($aleatorios_llegada); $i++)
			<?php $contador = $contador + 1;?>
			<tr>
				<th>{{ $contador }}</th>
				<td>
					<input type="text" class="form-control" name="aleatorio_llegada[]" id="llegada{{ $contador }}" value="{{ $aleatorios_llegada[$i] }}" />
				</td>
			</tr>
		@endfor
	</table>
</div>