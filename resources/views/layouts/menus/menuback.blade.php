 <!--  MENU PARA TODO TIPO DE USUARIO-->

 <li {{ (Request::is("inicio") ? 'class=active' : '') }}>
  <a href="{{ URL::asset('/') }}"><i class="fa fa-home" aria-hidden="true"></i>
    <span class="nav-label">Inicio</span>
  </a>
</li> 
<li {{ (Request::is(" ") ? 'class=active' : '') }}>
  <a href="#"><i class="fa fa-align-justify"></i> <span class="nav-label" >Métodos Randoms</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level">

   <li {{ (Request::is("numeros_aleatorios") ? 'class=active' : '') }}><a href="{{ URL::asset('numeros_aleatorios')}}" ></i>Congruencia Lineal</a></li>


 </ul>
</li>

<li {{ (Request::is(" ") ? 'class=active' : '') }}>
  <a href="#"><i class="fa fa-gear"></i> <span class="nav-label">Métodos Pronósticos</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level">
   <li {{ (Request::is("linea-espera") ? 'class=active' : '') }}>    
    <a href="{{ URL::asset('/promedio_movil') }}">
      <span class="nav-label">Promedio Móvil</span>
    </a>
  </li>

  <li {{ (Request::is("linea-espera") ? 'class=active' : '') }}>    
    <a href="{{ URL::asset('/alisamiento_exponencial') }}">
      <span class="nav-label">Alisamiento exponencial</span>
    </a>
  </li>

  <li {{ (Request::is("linea-espera") ? 'class=active' : '') }}>    
    <a href="{{ URL::asset('/regresion_lineal') }}">
      <span class="nav-label">Regresión Lineal</span>
    </a>
  </li>

</ul>
</li>

<li {{ (Request::is(" ") ? 'class=active' : '') }}>
  <a href="#"><i class="fa fa-gear"></i> <span class="nav-label">Métodos simulación</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level">
   <li {{ (Request::is("linea-espera") ? 'class=active' : '') }}>    
    <a href="{{ URL::asset('linea-espera') }}">
      <span class="nav-label">Linea de Espera</span>
    </a>
  </li>

  <li {{ (Request::is("montecarlo") ? 'class=active' : '') }}>    
    <a href="{{ URL::asset('montecarlo') }}">
      <span class="nav-label">Montecarlo</span>
    </a>
  </li>

  <li {{ (Request::is("inventario-montecarlo") ? 'class=active' : '') }}>    
    <a href="{{ URL::asset('inventario-montecarlo') }}">
      <span class="nav-label">Inventario Montecarlos</span>
    </a>
  </li>

  <li {{ (Request::is("linea-espera-montecarlo") ? 'class=active' : '') }}>    
    <a href="{{ URL::asset('linea-espera-montecarlo') }}">
      <span class="nav-label">Linea de Espera Montecarlos</span>
    </a>
  </li>

</ul>

<li >
  <a href=" "></i> <span class="nav-label" >Manual de Usuario</span> <span class="fa arrow"></span></a>
  <ul class="nav nav-second-level">

   <li {{ (Request::is(" ") ? 'class=active' : '') }}><a href="https://gitlab.com/lula02/tecnicas-de-simulacion/-/wikis/Manual-de-Usuario-T%C3%A9cnicas-de-simulaci%C3%B3n"><i class="fa fa-align-justify" ></i>Manual General</a></li>
  </a>
</li>