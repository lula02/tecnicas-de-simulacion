<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset='utf-8'/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="crsf-token" content="{{csrf_token()}}">

  <title>Técnias de Simulación</title>
  @yield('estilos')
  <!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">-->


  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.0/moment.min.js"></script>
  <!-- importo todos los idiomas -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.0/moment-with-locales.min.js"></script>

  {{-- <link rel="stylesheet" type="text/css" href="{{asset('admin/plantilla/css/bootstrap.min.css')}}"> --}}
  {!! Html::style('admin/plantilla/css/bootstrap.min.css') !!}
  {{-- <link href="{{ asset('local/vendor/kartik-v/bootstrap-fileinput/css/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css" /> --}}
  <link rel="stylesheet" type="text/css" href=" {{ asset('js/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }} ">
  {!! Html::style('admin/plantilla/font-awesome/css/font-awesome.css') !!}


  <!-- Toastr style -->
  {!! Html::style('admin/plantilla/css/plugins/toastr/toastr.min.css') !!}



  <!-- Gritter -->
  {!! Html::style('admin/plantilla/js/plugins/gritter/jquery.gritter.css') !!}


  {!! Html::style('admin/plantilla/css/animate.css') !!}
  {!! Html::style('admin/plantilla/css/style.css') !!}

  <!-- Data Tables -->
  {!! Html::style('admin/plantilla/css/plugins/dataTables/dataTables.bootstrap.css') !!}
  {!! Html::style('admin/plantilla/css/plugins/dataTables/dataTables.responsive.css') !!}
  {!! Html::style('admin/plantilla/css/plugins/dataTables/dataTables.tableTools.min.css') !!}

  {!! Html::style('admin/plantilla/css/plugins/blueimp/css/blueimp-gallery.min.css') !!}



  <!-- Mainly scripts -->
  {!! Html::script('admin/plantilla/js/jquery-2.1.1.js') !!}
  {!! Html::script('admin/plantilla/js/bootstrap.min.js') !!}
  {!! Html::script('admin/plantilla/js/plugins/metisMenu/jquery.metisMenu.js') !!}
  {!! Html::script('admin/plantilla/js/plugins/slimscroll/jquery.slimscroll.min.js') !!}

  {!! Html::script('admin/exports/tableToExcel.js') !!}

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css"/>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.css"/>
  <link rel="stylesheet" href="js/jquery-colorbox/example1/colorbox.css" /> 

  <!--datetimepicker-->
  <link rel="stylesheet" type="text/css" href="admin/datetime/css/bootstrap-datetimepicker.min.css">
  {!! HTML::style('admin/plantilla/css/plugins/chosen/chosen.css'); !!}

  
 {{--  Jeialert   
  <script type="text/javascript" src="{{asset('admin/plantilla/js/jalert/jquery.alerts.mod.js')}}"></script>    
  <link rel="stylesheet" href="{{asset('admin/plantilla/js/jalert/jquery.alerts.css')}}" />
     <script type="text/javascript" src="{{asset('admin/plantilla/js/jalert/jquery.ui.draggable.js')}}"></script>
              <script type="text/javascript" src="{{asset('admin/plantilla/js/jalert/jquery-1.4.2.min.js')}}"></script> --}}
       
            

  {{-- {!! Html::script('/assets/js/jquery.PrintArea.js') !!} 
  <script src="{{ asset('local/vendor/kartik-v/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
  <script src="{{ asset('local/vendor/kartik-v/bootstrap-fileinput/js/locales/es.js') }}"></script>  --}}
     
  <link rel="stylesheet" type="text/css" href="{{ asset('js/JqueryToast/jquery.toast.min.css') }}" />
</head>

<body>
  <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
          <li class="nav-header">                 

            <div class="dropdown profile-element">

              {{ HTML::image('photouleam.png','LOGO',array('style'=>'width: 180px;')) }} 


            </div>

            <div class="logo-element">
              TS
            </div>
          </li>

          <!--
          $useractual=\Auth::user(); 
-->
          @include('layouts.menus.menuback')

        </ul>

      </div>
    </nav>





    <div id="page-wrapper" class="gray-bg dashbard-1">
      <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
          <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>

          </div>
          <ul class="nav navbar-top-links navbar-right">








          </ul>

        </nav>
      </div>



      <div class="wrapper wrapper-content animated fadeInRight">           
        <div class="row">
          <div class="col-lg-12">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <h5>@yield('titulo') </h5>
                <div class="ibox-tools">
                  <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                  </a>


                </div>
              </div>
              <div class="ibox-content" class="container">
               @yield('contenido')
             </div>
           </div>
         </div>
       </div>
     </div>





   </div>



 </div>

 <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.js"></script>
  <!-- Color Box -->    
  <script type="text/javascript" src="{{asset('js/jquery-colorbox/jquery.colorbox.js')}}"></script>
  <!-- Data Tables -->
  {!! Html::script('admin/plantilla/js/plugins/dataTables/jquery.dataTables.js') !!}
  {!! Html::script('admin/plantilla/js/plugins/dataTables/dataTables.bootstrap.js') !!}
  {!! Html::script('admin/plantilla/js/plugins/dataTables/dataTables.responsive.js') !!}
  {!! Html::script('admin/plantilla/js/plugins/dataTables/dataTables.tableTools.min.js') !!}
  {!! Html::script('admin/plantilla/js/plugins/selective/selectize.js') !!}

  {!! Html::script('admin/plantilla/js/plugins/blueimp/jquery.blueimp-gallery.min.js') !!}

 <!-- Flot -->
  {!! Html::script('admin/plantilla/js/plugins/flot/jquery.flot.js') !!}
  {!! Html::script('admin/plantilla/js/plugins/flot/jquery.flot.tooltip.min.js') !!}
  {!! Html::script('admin/plantilla/js/plugins/flot/jquery.flot.spline.js') !!}
  {!! Html::script('admin/plantilla/js/plugins/flot/jquery.flot.resize.js') !!}
  {!! Html::script('admin/plantilla/js/plugins/flot/jquery.flot.pie.js') !!}


  {!! Html::script('admin/plantilla/js/plugins/flot/jquery.flot.pie.js') !!}


  <!-- Peity -->
  {!! Html::script('admin/plantilla/js/plugins/peity/jquery.peity.min.js') !!}
  {!! Html::script('admin/plantilla/js/demo/peity-demo.js') !!}

  <!-- Custom and plugin javascript -->
  {!! Html::script('admin/plantilla/js/inspinia.js') !!}
  {!! Html::script('admin/plantilla/js/plugins/pace/pace.min.js') !!}


  <!-- jQuery UI -->
  {!! Html::script('admin/plantilla/js/plugins/jquery-ui/jquery-ui.min.js') !!}


  <!-- GITTER -->
  {!! Html::script('admin/plantilla/js/plugins/gritter/jquery.gritter.min.js') !!}


  <!-- Sparkline -->
  {!! Html::script('admin/plantilla/js/plugins/sparkline/jquery.sparkline.min.js') !!}


  <!-- Sparkline demo data  -->
  {!! Html::script('admin/plantilla/js/demo/sparkline-demo.js') !!}


  <!-- ChartJS-->
  {!! Html::script('admin/plantilla/js/plugins/chartJs/Chart.min.js') !!}


  <!-- Toastr -->
  {!! Html::script('admin/plantilla/js/plugins/toastr/toastr.min.js') !!}

  <!-- Chosen -->
  {!! Html::script('admin/plantilla/js/plugins/chosen/chosen.jquery.js') !!}
  {{-- {!! Html::script('/assets/js/blazy.min.js') !!} --}}
 <script type="text/javascript">

  var plugin_path = 'assets/plugins/';

  var bLazy = new Blazy({
    selector: 'img'
  });

</script>

<script type="text/javascript">
  $("#input-image").fileinput({
    language: 'es',
    browseLabel: "Examinar",
    browseClass: "btn btn-success",
    showUpload: false,
    maxFileCount: 10,
    showRemove: false,
    maxFileSize: 8192,
    browseIcon:'<i class="fa fa-user" aria-hidden="true"></i>',
    layoutTemplates: {
      main1: "{preview}\n" +
      "<div class=\'input-group {class}\'>\n" +
      "   <div class=\'input-group-btn\'>\n" +
      "       {browse}\n" +
      "       {upload}\n" +
      "       {remove}\n" +
      "   </div>\n" +
      "   {caption}\n" +
      "</div>"
    }
  });
  $("#file_cedula").fileinput({
   language: 'es',
   browseLabel: "Examinar",
   browseClass: "btn btn-success",
   showUpload: false,
   maxFileCount: 10,
   maxFileSize: 8192,
   browseIcon:"<i class='fa fa-file-pdf-o' aria-hidden='true'></i>",
   showRemove: false,
   allowedFileExtensions: ["pdf", "PDF"],
      //uploadUrl: '/file-upload-batch/2',
      //maxFilePreviewSize: 8000
      layoutTemplates: {
        main1: "{preview}\n" +
        "<div class=\'input-group {class}\'>\n" +
        "   <div class=\'input-group-btn\'>\n" +
        "       {browse}\n" +
        "       {upload}\n" +
        "       {remove}\n" +
        "   </div>\n" +
        "   {caption}\n" +
        "</div>"
      }
    });


  function valor(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==8) return true;
    var patron =/[0-9.,]/;
    var te = String.fromCharCode(tecla);
    return patron.test(te);
  }
  function solonum(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==8) return true;
    var patron =/[0-9]/;
    var te = String.fromCharCode(tecla);
    return patron.test(te);
  }
  function numyletra(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==8) return true;
    var patron =/[0-9A-Za-z]/;
    var te = String.fromCharCode(tecla);
    return patron.test(te);
  }
/*

   function calcularedad(birth_month, birth_day, birth_year) {

    <?php 
      date_default_timezone_set('America/Guayaquil');
      ?>

    today_date = new Date();
    today_year = today_date.getFullYear();
    today_month = today_date.getMonth();
    today_day = today_date.getDate();
    age = today_year - birth_year;

    if (today_month < (birth_month - 1)) {
      age--;
    }
    if (((birth_month - 1) == today_month) && (today_day < birth_day)) {
      age--;
    }
    
    if(age<1)
    {
      var meses=0;
      if(today_month>birth_month)
          meses=today_month-birth_month;
      if(today_month<birth_month)
          meses=12-(birth_month-ahora_mes);
      if(today_month==birth_month && birth_day>today_day)
          meses=11;

    }
  } */
  function calcularedadd(mes,dia,ano)
  {
    <?php 
    date_default_timezone_set('America/Guayaquil');
    ?>

      // cogemos los valores actuales
      var fecha_hoy = new Date();
      var ahora_ano = fecha_hoy.getYear();
      var ahora_mes = fecha_hoy.getMonth()+1;
      var ahora_dia = fecha_hoy.getDate();

      // realizamos el calculo
      var edad = (ahora_ano + 1900) - ano;
      if ( ahora_mes < mes )
      {
        edad--;
      }
      if ((mes == ahora_mes) && (ahora_dia < dia))
      {
        edad--;
      }
      if (edad > 1900)
      {
        edad -= 1900;
      }

      // calculamos los meses
      var meses=0;
      if(ahora_mes>mes)
        meses=ahora_mes-mes;
      if(ahora_mes<mes)
        meses=12-(mes-ahora_mes);
      if(ahora_mes==mes && dia>ahora_dia)
        meses=11;

      // calculamos los dias
      var dias=0;
      if(ahora_dia>dia)
        dias=ahora_dia-dia;
      if(ahora_dia<dia)
      {
        ultimoDiaMes=new Date(ahora_ano, ahora_mes, 0);
        dias=ultimoDiaMes.getDate()-(dia-ahora_dia);
      }

      return edad+" años, "+meses+" meses y "+dias+" días";

    }


    function solonum(e) {
      tecla = (document.all) ? e.keyCode : e.which;
      if (tecla==8) return true;
      var patron =/[0-9]/;
      var te = String.fromCharCode(tecla);
      return patron.test(te);
    }
    function sololet(e) {
      tecla = (document.all) ? e.keyCode : e.which;
      if (tecla==8) return true;
      var patron =/[A-Za-z\sñ]/;
      var te = String.fromCharCode(tecla);
      return patron.test(te);
    }

    function soloemail(e) {
      tecla = (document.all) ? e.keyCode : e.which;
      if (tecla==8) return true;
      var patron =/[^\s]/;
      var te = String.fromCharCode(tecla);
      return patron.test(te);

    }



  </script> 
  <script type="text/javascript" src="{{asset('js/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/bootstrap-datepicker/locales/bootstrap-datepicker.es.js')}}"></script>
  
  {{-- <script src="{{ asset('local/public/vendors/ckeditor/ckeditor.js') }}"></script> --}}


  <!-- Time picker -->
  <script type="text/javascript" src="{{asset('js/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>

  {!! HTML::style('admin/plantilla/css/plugins/selective/selectize.bootstrap3.css'); !!}



  <script src="https://use.fontawesome.com/a2263a18cb.js"></script>



  {!! Html::script('js/chartjs/Chart.bundle.js') !!}

  <script>
    $(document).ready(function() {
      $('#tbbuzon').dataTable({
        responsive : true,
        language: {
          "emptyTable":     "No hay datos disponibles en la tabla",
          "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
          "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
          "infoFiltered":   "(filtered from _MAX_ total entries)",
          "infoPostFix":    "",
          "thousands":      ",",
          "lengthMenu":     "Mostrar _MENU_ entradas",
          "loadingRecords": "Cargando...",
          "processing":     "Procesando...",
          "search":         "Buscar:",
          "zeroRecords":    "No se encontraron registros coincidentes",
          "paginate": {
            "first":      "Primero",
            "last":       "Último",
            "next":       "Siguiente",
            "previous":   "Atrás"
          },
          "aria": {
            "sortAscending":  ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
          }
        },

        "order": ([ 0, 'desc' ]),

        responsive: true,
        dom: 'Bfrtip',
        buttons: [
        'copyHtml5',
        'excelHtml5'
        ]


      });


      $('#tbbuzon1').dataTable({
        responsive : true,
        language: {
          "emptyTable":     "No hay datos disponibles en la tabla",
          "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
          "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
          "infoFiltered":   "(filtered from _MAX_ total entries)",
          "infoPostFix":    "",
          "thousands":      ",",
          "lengthMenu":     "Mostrar _MENU_ entradas",
          "loadingRecords": "Cargando...",
          "processing":     "Procesando...",
          "search":         "Buscar:",
          "zeroRecords":    "No se encontraron registros coincidentes",
          "paginate": {
            "first":      "Primero",
            "last":       "Último",
            "next":       "Siguiente",
            "previous":   "Atrás"
          },
          "aria": {
            "sortAscending":  ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
          }
        },

        "order" : ([0,'desc']),
            /*
                  "order": ([ 0, 'desc' ]),

                  responsive: true,
                  dom: 'Bfrtip',*/
      /*buttons: [
          'copyHtml5',
          'excelHtml5'
          ]*/


        });

    });  

  </script>
  <script>

    $(document).ready(function() {

      var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
        '.chosen-select-width'     : {width:"100%"}
      }
      for (var selector in config) {
        $(selector).chosen(config[selector]);
      }

    });     

  </script>  
  <script src="{{asset('js/bootstrap-filestyle.min.js')}}"></script>    
  <script src="{{ asset('admin/datetime/js/bootstrap-datetimepicker.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/JqueryToast/jquery.toast.min.js') }}"></script>

  <script src="{{ mix('js/app.js') }}"></script>

  @yield('script') 


</body>
</html>
