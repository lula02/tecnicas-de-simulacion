@extends('layouts.backend')
@section('ruta')
@stop
@section('titulo')
Promedio Movil
@stop
@section('contenido')
@include('alerts.success')

<div class="panel panel-primary" id="app">
	<div class="panel-heading" style="text-align: center;">
		<h3>Ingrese los datos para continuar</h3>
	</div>
	
	<promedio-movil></promedio-movil>

</div>

<br>
<br>
<div class="row">
	<div class="col-md-12">
		<div id="tabla">
		</div>
	</div>
</div>


@stop
@section('script')
<script type="text/javascript">
	function calcular_aleatorio() {
		var datos = $('#formulario').serialize();
		console.log(datos);
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: '{{ url('generar_tablas_aleatorios') }}',
			type: 'GET',
			data: datos,
		})
		.done(function(datox) {
			$('#tabla').html(datox);
		})
		.fail(function(error) {
			console.log(error);
		})
		.always(function() {
			console.log("complete");
		});

	}
</script>
@endsection


