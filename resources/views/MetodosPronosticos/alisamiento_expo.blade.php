@extends('layouts.backend')
@section('ruta')

@section('titulo')
Alisamiento Exponencial
@endsection

@section('contenido')
@include('alerts.success')

<div class="panel panel-primary" id="app">
	<div class="panel-heading" style="text-align: center;">
		<h3>Ingrese los datos para continuar</h3>
	</div>
	
	<alisamiento-exponencial></alisamiento-exponencial>

</div>

<br>
<br>
<div class="row">
	<div class="col-md-12">
		<div id="tabla">
		</div>
	</div>
</div>


@endsection


