@extends('layouts.backend')
@section('ruta')
@stop
@section('titulo')
MONTECARLOS
@stop
@section('contenido')
@include('alerts.success')
	<div class="panel panel-primary">
	  <div class="panel-heading" style="text-align: center;">
	  	<h3>Ingrese los datos para continuar</h3>
	  </div>
	  <div class="panel-body">
	  	<form id="formulario">
	  		<div class="row">
	  			<div class="col-md-6">
	  				<label for="eventos">Numero de eventos para demandas y probabilidades</label>
	  				<div class="input-group">
				      <input type="text" name="eventos" id="eventos" class="form-control" placeholder="Ingrese el numero de eventos">
				      <span class="input-group-btn">
				        <button class="btn btn-primary" onclick="return generar_aleatorios();" type="button">Generar Campos</button>
				      </span>
				    </div>
				    <div id="llegadaservicio"></div>
	  			</div>
	  			
	  			<div class="col-md-6">
	  				<label for="nllegadas">Ingrese el numero de eventos para Simulación</label>
	  				<div class="input-group">
				      <input type="text" name="nllegadas" id="nllegadas" class="form-control" placeholder="Ingrese el numero de llegadas">
				      <span class="input-group-btn">
				        <button class="btn btn-primary" onclick="return generar_tab_pro();" type="button">Generar Eventos</button>
				      </span>
				    </div>
				    <div id="llegadas"></div>
	  			</div>
	  		</div>
		</form>
	  </div>
	</div>

	<br>
	<br>
	<div class="row">
		<div class="col-md-12" id="lineamontecarlocalculo"></div>
	</div>
@stop
@section('script')
<script type="text/javascript">

		function generar_aleatorios() {
		var nllegadas = $('#eventos').val();
			$.ajax({
				headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
					url: '{{ url('vista_demanda_prob') }}',
				type: 'GET',
				data: {'cantidad':nllegadas},
			})
			.done(function(datox) {
			$('#llegadaservicio').html(datox);
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		}

		function generar_tab_pro() {
			var nllegadas = $('#nllegadas').val();
			$.ajax({
				headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
				url: '{{ url('generar_aleatoriosx') }}',
				type: 'GET',
				data: {'eventos':nllegadas},
				
			})
			.done(function(datox) {
				$('#llegadas').html(datox);
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		}
		function calcular_linea_espera() {
			var datos = $('#llegadaservicio,#llegada, #servicio, #probabilidad1, #probabilidad2').find('input').serialize();
			$.ajax({
				headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
				url: '{{ url('linea-montecarlo-calculox') }}',
				type: 'GET',
				data: datos,
			})
			.done(function(datox) {
				$('#lineamontecarlocalculo').html(datox);
			})
			.fail(function(error) {
				console.log(error);
			})
			.always(function() {
				console.log("complete");
			});
		}

	</script>
@endsection


