@extends('layouts.backend')
@section('ruta')
@stop
@section('titulo')
LINEA DE ESPERA
@stop
@section('contenido')
@include('alerts.success')
<div class="panel panel-primary">
	  <div class="panel-heading" style="text-align: center;">
	  	<h3>Ingrese los datos para continuar</h3>
	  </div>
	  <div class="panel-body">
	  	<form id="formulario">
	  		<div class="row">
	  			<div class="col-md-6">
	  				<label for="eventos">Ingrese el numero de eventos</label>
	  				<div class="input-group">
				      <input type="text" name="eventos" id="eventos" class="form-control" placeholder="Ingrese el numero de eventos">
				      <span class="input-group-btn">
				        <button class="btn btn-primary" onclick="return generar_aleatorios();" type="button">Generar Campos</button>
				      </span>
				    </div>
	  			</div>

	  			<div class="col-md-3">
	  				<label for="lamda">Ingrese landa</label>
				    <input type="text" name="lamda" id="lamda" class="form-control" placeholder="Ingrese el valor de lamda">
	  			</div>

	  			<div class="col-md-3">
	  				<label for="niu">Ingrese nu</label>
				    <input type="text" name="niu" id="niu" class="form-control" placeholder="Ingrese el valor de niu">
	  			</div>
	  		</div>
		    
		</form>
	  </div>
	</div>

	<br>
	<br>
	<div class="row">
		<div class="col-md-12">
			<div id="llegadaservicio">
			</div>
		</div>
	</div>

	<br>
	<br>
	<div class="row">
		<div class="col-md-12">
			<div id="tablacalculada">
			</div>
		</div>
	</div>
	<div class="row">
		<div style="width: 1000px;">
			
		
<canvas id="myChart"></canvas>
</div>
		
	</div>

@stop
@section('script')


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>



<script type="text/javascript">




		function generar_aleatorios() {
			var eventos = $('#eventos').val();
			$.ajax({
				headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
				url: '{{ url('generar_aleatorios') }}',
				type: 'GET',
				data: {'eventos':eventos},
			})
			.done(function(datox) {
				$('#llegadaservicio').html(datox);
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			
		}

		function calcular_linea_espera() {
			var datos = $('#llegada, #servicio').find('input').serialize();
			var lamda = $('#lamda').val();
			var niu = $('#niu').val();
			$.ajax({
				headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
				url: '{{ url('linea-espera-calculo') }}',
				type: 'GET',
				data: datos+'&lamda='+lamda+'&niu='+niu,
			})
			.done(function(datox) {
				$('#tablacalculada').html(datox[0]);

				var ctx = $('#myChart');
				var arreglolabel = [];
				var arreglodata = [];
				var arreglonume = [];
				var arreglonu = [];
				var datos = datox[1];
					
			for (var i = 0;  i < datos.length ; i++) {
					arreglolabel.push(datos[i][2]);
					arreglodata.push({
						label: datos[i][9], 
						data:datos[i][9]
					});
					arreglonume.push(datos[i][9]);
					arreglonu.push(datos[i][6]);

				}

//console.log(arreglonu);
      
          var ctx = document.getElementById('myChart').getContext('2d');
          var myChart = new Chart(ctx, {
            type: 'line',
            data: {
               
              labels: arreglonume,

              datasets: [{
                label: 'Tiempo en el sistema',
                data: arreglonume,
                backgroundColor: "rgba(26,129,102,0.2)",
                borderColor: "#3cba9f",
                //fill:false
              }, {
                label: 'Hora de llegada exacta',
                data: arreglonu,
                backgroundColor: "rgba(255,153,0,0.2)",
                borderColor: "rgba(179,11,198,1)",
                //fill:false
              }]
            
            }

          });
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		}



  

	</script>
@endsection


