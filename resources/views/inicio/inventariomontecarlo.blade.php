@extends('layouts.backend')
@section('ruta')
@stop
@section('titulo')
SISTEMA DE INVENTARIO MONTECARLOS
@stop
@section('contenido')
@include('alerts.success')
	<div class="panel panel-primary">
	  <div class="panel-heading" style="text-align: center;">
	  	<h3>Ingrese los datos de la politica del Ejercicio</h3>
	  </div>
	  <div class="panel-body">
	  	<form id="formulario">
	  		<div class="row">
	  			<div class="col-md-6">
	  				<div class="form-group">
		  				<label for="r">Ingrese el valor para el punto de reorden(R)</label>
					    <input type="text" name="r" id="r" class="form-control" placeholder="Ingrese el valor para R">
				    </div>
	  			</div>

	  			<div class="col-md-6">
	  				<div class="form-group">
		  				<label for="q">Ingrese el valor para Q</label>
					    <input type="text" name="q" id="q" class="form-control" placeholder="Ingrese el valor para Q">
				    </div>
	  			</div>
	  		</div>
		    
		    <div class="row">
	  			<div class="col-md-6">
	  				<div class="form-group">
		  				<label for="inv_ini">Ingrese el valor para inventario inicial</label>
					    <input type="text" name="inv_ini" id="inv_ini" class="form-control" placeholder="Ingrese el valor para inventario inicial">
				    </div>
	  			</div>

	  			<div class="col-md-6">
	  				<div class="form-group">
		  				<label for="ch">Ingrese el valor para el costo de mantenimiento(Ch)</label>
					    <input type="text" name="ch" id="ch" class="form-control" placeholder="Ingrese el valor para Ch">
				    </div>
	  			</div>
	  		</div>

	  		<div class="row">
	  			<div class="col-md-6">
	  				<div class="form-group">
		  				<label for="co">Ingrese el valor para el costo de ordenar(Co)</label>
					    <input type="text" name="Co" id="co" class="form-control" placeholder="Ingrese el valor para Co">
				    </div>
	  			</div>

	  			<div class="col-md-6">
	  				<div class="form-group">
		  				<label for="cf">Ingrese el valor para Cf</label>
					    <input type="text" name="cf" id="cf" class="form-control" placeholder="Ingrese el valor para Cf">
				    </div>
	  			</div>
	  		</div>

	  		<div class="row">
	  			<div class="col-md-4">
	  				<label for="eventos">Número de eventos para llegadas y salidas</label>
	  				<div class="input-group">
				      <input type="text" name="eventos" id="eventos" class="form-control" placeholder="Ingrese el numero de eventos">
				      <span class="input-group-btn">
				        <button class="btn btn-primary" onclick="return generar_aleatorios();" type="button">Generar Campos</button>
				      </span>
				    </div>
				    <div id="aleatorios"></div>
	  			</div>
	  			
	  			<div class="col-md-4">
	  				<label for="ndemanda">Ingrese el número de eventos de Demandas</label>
	  				<div class="input-group">
				      <input type="text" name="ndemanda" id="ndemanda" class="form-control" placeholder="Ingrese el numero de demandas" />
				      <span class="input-group-btn">
				        <button class="btn btn-primary" onclick="return generar_tab_pro(3);" type="button">Generar demandas</button>
				      </span>
				    </div>
				    <div id="demandas"></div>
	  			</div>
		    
	  			<div class="col-md-4">
	  				<label for="nretraso">Ingrese el número de eventos de Retraso</label>
	  				<div class="input-group">
				      <input type="text" name="nretraso" id="nretraso" class="form-control" placeholder="Ingrese el numero de retrasos" />
				      <span class="input-group-btn">
				        <button class="btn btn-primary" onclick="return generar_tab_pro(4);" type="button">Generar retrasos</button>
				      </span>
				    </div>
				    <div id="retrasos"></div>
	  			</div>
	  		</div>
		</form>
	  </div>
	</div>

	<br><br>

	<div class="row">
		<div class="col-md-12">
			<div id="resuelta"></div>
		</div>
	</div>
@stop
@section('script')
<script type="text/javascript">
		function generar_aleatorios() {
			var eventos = $('#eventos').val();
			$.ajax({
				headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
				url: '{{ url('generar_aleatorios') }}',
				type: 'GET',
				data: {'eventos':eventos, 'tipo':2},
			})
			.done(function(datox) {
				$('#aleatorios').html(datox);
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		}

		function generar_tab_pro(tipo) {
			var ndemandas = $('#ndemanda').val();
			var nretrasos = $('#nretraso').val();
			var aenviar = ndemandas;
			if (tipo == 4) {
				aenviar = nretrasos;
			}
			$.ajax({
				headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
				url: '{{ url('probabilidades_llegada') }}',
				type: 'GET',
				data: {'cantidad':aenviar, 'tipo':tipo},
			})
			.done(function(datox) {
				if (tipo == 3) {
					$('#demandas').html(datox);
				}
				if (tipo == 4) {
					$('#retrasos').html(datox);
				}
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		}

		function calcular_linea_espera() {
			var datos = $('#formulario').serialize();
			$.ajax({
				headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
				url: '{{ url('inventario_calculo') }}',
				type: 'GET',
				data: datos,
			})
			.done(function(datox) {
				$('#resuelta').html(datox);
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			
		}
	</script>
@endsection


