<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>GAD Manta | 404 Error</title>

    {!! Html::style('admin/plantilla/css/bootstrap.min.css') !!}
    {!! Html::style('admin/plantilla/font-awesome/css/font-awesome.css') !!}

     {!! Html::style('admin/plantilla/css/animate.css') !!}
    {!! Html::style('admin/plantilla/css/style.css') !!}
</head>

<body class="gray-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1>404</h1>
        <h3 class="font-bold">Página no encontrada</h3>

        <div class="error-desc">
            
Lo sentimos, pero se ha encontrado la página que está buscando. Intente comprobar la dirección URL para el error, después haga clic en el botón de actualización de su navegador o probar encontrado algo más en nuestra aplicación.            

        </div>
        <br>
<p>
<a href="{{ URL::previous() }}" class="btn btn-primary ">Regresar</a>
</p>
    </div>

    <!-- Mainly scripts -->
    {!! Html::script('admin/plantilla/js/jquery-2.1.1.js') !!}
     {!! Html::script('admin/plantilla/js/bootstrap.min.js') !!}
     

</body>

</html>
