@extends('layouts.backend')

@section('ruta')
Gertor CMS FIXSYSTEM
@stop

@section('titulo')
INICIO
@stop

@section('contenido')
@include('alerts.success')
<?php 

$acep = Auth::User();
?>


<style> 
  #foto_modificar
  {
    position: absolute;
    opacity: 0;
  }


  #foto_modificar:hover {
    position: absolute;
    opacity: 10;
  }

</style>


<div class="wrapper wrapper-content animated fadeInRight">


  <div class="text-center">
    <h1 class="display-2"> 
      Técnicas de Simulación
    </h1>
    <h1 class="contentPara">Ing. Jorge Moya Delgado</h1>  
    <h3 class="contentPara"> 0ctavo "A"</h3>

  </div>
  <div class="row">
    <div class="text-center">
      <div class="form-group col-md-6 col-lg-6">
        <h1 class="logo-name"> 
          <img src="{{asset('genesis.jpeg')}}" >
        </h1>
        <h3 class="contentPara">Carreño Macías Génesis Monserrate</h3>  
        <h3 class="contentPara">e1350495808@live.uleam.edu.ec</h3>
      </div>

      <div class="form-group col-lg-6 col-md-6">
        <h1 class="logo-name"> 
          <img src="{{asset('ludys.jpeg')}}" >
        </h1>
        <h3 class="contentPara">Mendoza Santana Ludys Sofía</h3>  
        <h3 class="contentPara">e1316681343@live.uleam.edu.ec</h3>
      </div>
<h3 class="contentPara"> Realizado en PHP-LARAVEL - VueJS-NODE </h3>
    </div>
  </div>








</div>        
</div>


@stop
@section('script')
<script type="text/javascript">
  $(document).ready(function() { 
  });
  function Actualizar_foto_pop()
  {
    $('#pop_up_cambio_foto').modal('show');
  }
  function randomColorFactor()
  {
    return Math.round(Math.random()*255);
  }
  function randomColor()
  {
    return 'rgba('+randomColorFactor()+','+randomColorFactor()+','+randomColorFactor()+',.7)';
  }
</script>
@endsection


